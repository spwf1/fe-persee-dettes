import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [{
  path: '',
  loadChildren: () =>
    import('./components/dettes.module').then(
      (m) => m.DettesModule
    ),
  pathMatch: 'full',
}];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
