import {Component, ViewChild, ViewContainerRef} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-home-dettes',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {

  constructor(private translateService: TranslateService,
              private httpClient: HttpClient) {
    translateService.use('en');
    translateService.instant('HELLO');
    this.httpClient.get('/assets/i18n/en.json')
      .subscribe(res => console.log(res));
  }
}
