import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomeComponent} from "./home/home.component";
import {DettesRoutingModule} from "./dettes-routing.module";
import {FePerseeCommunModule} from "fe-persee-commun";
import {SharedComponent} from "./shared/shared.component";
import { DettesSearchComponent } from './dettes-search/dettes-search.component';
import { DetteResultComponent } from './dette-result/dette-result.component';
import { ExternalTabDettesComponent } from './external-tab-dettes/external-tab-dettes.component';
import { SubDetteTestComponent } from './sub-dette-test/sub-dette-test.component';
import {TranslateLoader, TranslateModule, TranslateStore} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";

export function HttpLoaderFactory(http: HttpClient) {
  console.log("load dette translation");
  http.get('./assets/i18n/dettes/en.json').subscribe(res => console.log(res));
  return new TranslateHttpLoader(http, './assets/i18n/dettes/', '.json');
}

@NgModule({
  declarations: [
    HomeComponent,
    SharedComponent,
    DettesSearchComponent,
    DetteResultComponent,
    ExternalTabDettesComponent,
    SubDetteTestComponent
  ],
    imports: [
        CommonModule,
        DettesRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        FePerseeCommunModule,
        TranslateModule.forChild({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
    ],
  providers: [TranslateStore]
})
export class DettesModule {}
