import {Component, OnInit} from '@angular/core';
import {SharedService} from "fe-persee-commun";

@Component({
  selector: 'app-dettes-search',
  templateUrl: './dettes-search.component.html',
  styleUrls: ['./dettes-search.component.css']
})
export class DettesSearchComponent implements OnInit {
  searchCriteria = "";

  constructor(private sharedService: SharedService) {
  }

  ngOnInit(): void {
    if (this.sharedService.getData()) {
      this.searchCriteria = this.sharedService.getData();
    }
  }

  searchDette() {
    this.sharedService.setData(this.searchCriteria);
  }

  clear() {
    this.searchCriteria = "";
    this.sharedService.setData("");
  }

  getStoredData(): string {
    return this.sharedService.getData();
  }
}
