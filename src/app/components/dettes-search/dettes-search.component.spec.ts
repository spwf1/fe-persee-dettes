import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DettesSearchComponent } from './dettes-search.component';

describe('DettesSearchComponent', () => {
  let component: DettesSearchComponent;
  let fixture: ComponentFixture<DettesSearchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DettesSearchComponent]
    });
    fixture = TestBed.createComponent(DettesSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
