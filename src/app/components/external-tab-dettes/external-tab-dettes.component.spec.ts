import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalTabDettesComponent } from './external-tab-dettes.component';

describe('ExternalTabDettesComponent', () => {
  let component: ExternalTabDettesComponent;
  let fixture: ComponentFixture<ExternalTabDettesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ExternalTabDettesComponent]
    });
    fixture = TestBed.createComponent(ExternalTabDettesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
