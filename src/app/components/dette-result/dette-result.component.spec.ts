import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetteResultComponent } from './dette-result.component';

describe('DetteResultComponent', () => {
  let component: DetteResultComponent;
  let fixture: ComponentFixture<DetteResultComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetteResultComponent]
    });
    fixture = TestBed.createComponent(DetteResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
