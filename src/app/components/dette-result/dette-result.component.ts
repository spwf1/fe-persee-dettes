import {Component, OnDestroy} from '@angular/core';
import {TransversalSubMenuService} from "fe-persee-commun";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-dette-result',
  templateUrl: './dette-result.component.html',
  styleUrls: ['./dette-result.component.css']
})
export class DetteResultComponent implements OnDestroy {

  readonly componentId = "test-component";

  constructor(private subMenuService: TransversalSubMenuService,
              private route: ActivatedRoute) {
    this.subMenuService.emitEvent(this.componentId);
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.subMenuService.setEntity(id);
      this.subMenuService.currentComponentInstance = this;
    });
  }

  ngOnDestroy(): void {
    this.subMenuService.currentComponentInstance = undefined;
  }
}
