import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {DettesSearchComponent} from "./dettes-search/dettes-search.component";
import {DetteResultComponent} from "./dette-result/dette-result.component";
import {SubDetteTestComponent} from "./sub-dette-test/sub-dette-test.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'dettes-search', component: DettesSearchComponent},
  {path: 'dettes-result/:id', component: DetteResultComponent},
  {path: 'dettes-sub/:id', component: SubDetteTestComponent}
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class DettesRoutingModule {
}
