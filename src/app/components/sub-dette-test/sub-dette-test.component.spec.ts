import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDetteTestComponent } from './sub-dette-test.component';

describe('SubDetteTestComponent', () => {
  let component: SubDetteTestComponent;
  let fixture: ComponentFixture<SubDetteTestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SubDetteTestComponent]
    });
    fixture = TestBed.createComponent(SubDetteTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
