import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-shared-creance',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.scss'],
})
export class SharedComponent implements OnInit {

  @Input() testInput: string = "Pas passé";

  constructor() {}

  ngOnInit(): void {}
}
