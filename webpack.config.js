const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, 'tsconfig.json'),
  [/* mapped paths to share */]);

module.exports = {
  output: {
    uniqueName: "dettesConfig",
    publicPath: "auto"
  },
  optimization: {
    runtimeChunk: false
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  experiments: {
    outputModule: true
  },
  plugins: [
    new ModuleFederationPlugin({
      library: { type: "module" },

      // For remotes (please adjust)
      name: "dettesConfig",
      filename: "remoteEntry.js",
      exposes: {
        './Module' : './src/app/components/dettes.module.ts',
        MenuEntry : './src/app/components/dettes-menu.json',
        SharedComponentFromDettes : './src/app/components/shared/shared.component.ts',
        SubMenuEntry: './src/app/components/dettes-sub-menu.json',
        TabMenu: './src/app/components/dettes-tab-menu.json',
        ExternalTabDettesComponent: './src/app/components/external-tab-dettes/external-tab-dettes.component.ts'
      },

      // For hosts (please adjust)
      // remotes: {
      //    "fePerseePortail": "http://localhost:4201/remoteEntry.js",

      //  },

      shared: share({
        "@angular/core": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
        "@angular/common": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
        "@angular/common/http": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
        "@angular/router": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
        "fe-persee-commun": { singleton: false, strictVersion: false, requiredVersion: 'auto' },
        "@ngx-translate/core": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
        "@ngx-translate/http-loader": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
        ...sharedMappings.getDescriptors()
      })

    }),
    sharedMappings.getPlugin()
  ],
};
